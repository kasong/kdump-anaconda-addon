#!/bin/sh

# Install from pip since they are missing in RHEL
pip install --ignore-installed pylint nose

# Run test
make test
